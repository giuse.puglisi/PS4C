
import astropy
from astropy import units as u,  constants as C
import numpy as np
import pylab as pl
import sys

import ps4c
import os

from ps4c.experiment import Experiment
from ps4c.forecaster import Forecaster


workdir =os. getcwd()+'/'
try :
    os.makedirs ( workdir +'test')
except FileExistsError :
    print(f"Overwriting in { workdir +'test'} " )

verbose =False
a=Experiment(fwhm=4., ID='TEST', frequency=20,
sensitivity=.0005,  units_beam='arcmin', units_sensitivity='mJy',fsky=.01)
fc= Forecaster(a ,ps4c_dir=workdir , sigmadetection=5.)
fc.forecast_pi2scaling(bestfitparams2file=False ,verbose=verbose  , exclude_HFI_data=True  )

fc(model='T11')
fc.print_info()

a=Experiment(fwhm=4.  , ID='TEST', frequency=220 ,
sensitivity=5. ,  units_beam='arcmin', units_sensitivity='uKarcmin',fsky=.01)
fc= Forecaster(a ,ps4c_dir=workdir , sigmadetection=5.)
fc.forecast_pi2scaling(bestfitparams2file=False ,verbose=verbose  , exclude_HFI_data=False )
fc(model='T11')
fc ()
fc.print_info()

a= Experiment(fwhm=[4., 3.5,3.5,2. ], ID='TEST', frequency=[90., 150,217, 340],
  sensitivity=[5.,10,20., 25.],  units_beam='arcmin', units_sensitivity='uKarcmin',fsky=.01)
fc= Forecaster(a ,ps4c_dir=workdir , sigmadetection=5.)
fc.forecast_pi2scaling(bestfitparams2file=False ,verbose=verbose  , exclude_HFI_data=False )
fc(model='T11')
fc.print_info()

fc()
fc.print_info()
fc.plot_integral_numbercounts(savefig=workdir+ '/test/numbercounts.png')
fc.plot_powerspectra(spectra_to_plot='Bonly', savefig=workdir +  '/test/powerspectra.png' )

a=Experiment(fwhm=[4.,  2. ], ID='TEST', frequency=[20,  340],
sensitivity=[5., 25.],  units_beam='arcmin', units_sensitivity='uKarcmin',fsky=.01)
fc= Forecaster(a ,ps4c_dir=workdir , sigmadetection=5.)
fc.forecast_pi2scaling(bestfitparams2file=False ,verbose=verbose  , exclude_HFI_data=False )
fc(model='T11')
#print fc.A, fc.B
#fc()
fc.print_info()
