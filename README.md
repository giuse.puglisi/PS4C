# Point Source ForeCast
This repository  contains the Point Source ForeCast (PS4C) package aimed at forecasting the contribution of detected and undetected extra-galactic radio sources
to Cosmic Microwave Background measurements. Both intensity and polarized emissions are considered and the forecast deliver the level of contamination up to the 
angular power spectrum level. Documentation is online at <http://giuspugl.github.io/ps4c/index.html> .

