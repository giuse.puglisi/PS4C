# -*- coding: utf-8 -*-
"""ps4c """

from setuptools import setup

setup(name='ps4c',
      version='2.0',
      description='Point Source Forecasts',
      author='Giuseppe Puglisi',
      author_email='gpuglisi@lbl.gov',
      packages=['ps4c'],
      python_requires='>3',
      include_package_data=True
      )
