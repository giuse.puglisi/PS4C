#
#   experiment.py
#   module encoding the forecasting package.
#
#   date: 2020-05-21
#   author: GIUSEPPE PUGLISI
#
#   Copyright (C) 2017   Giuseppe Puglisi
#


import pylab as pl
import healpy as hp
import numpy as np
import h5py as h5
import string
import astropy
from astropy import units as u, constants as C
from astropy.modeling.blackbody import blackbody_nu
from .IO import bash_colors
from .pointsource_utilities import beamsolidangle


class Experiment(object):
    """
    Initialize a class encodes all the nominal specifics of an experiment, i.e. resolution, sensitivity, size of the observational area,
    frequency channels.

    **Parameters**

    - `ID`:{string}
        name of the experiment, needed for plotting and printing info.
    - `frequency`:{float or sequence}
        the frequency of channels (assumed GHz)
    - `fwhm`:{float or sequence}
        the the Full Width at Half Maximum resolution, units can be deg, arcmin, or arcsec, set the keyword argument ``units_beam``,
        (Default= ``arcmin``)
    - `sensitivity`:{float or sequence}
        the sensitivity at each channel. Default units are :math:`\mu K arcmin`, but one can change different units by the ``units_sensitivity``
        keyword.
    - `fsky`:{float}
        percentage of the sky observed by the experiment.

    .. note::

        If ``units_sensitivity=uKsqrts`` then another keyword must be set, the observational time.

    """

    def get_pixelsize(self):
        """
        Given the fwhm, it computes the optimal (Healpix) pixelization  such that the pixel  size is about 3 times smaller than the resolution.
        """
        nsides = np.power(2, np.arange(5, 17))
        if self.nchans > 1:
            pixsize = []
            for fwhm in self.fwhm:

                nside = nsides[
                    np.argmin(
                        [
                            abs(fwhm.value / 3 - hp.nside2resol(n, arcmin=True))
                            for n in nsides
                        ]
                    )
                ]
                pixsize.append(hp.nside2resol(nside, arcmin=True))
        else:
            nside = nsides[
                np.argmin(
                    [
                        abs(self.fwhm.value / 3 - hp.nside2resol(n, arcmin=True))
                        for n in nsides
                    ]
                )
            ]
            pixsize = hp.nside2resol(nside, arcmin=True)
        (pixsize) *= u.arcmin
        return pixsize

    def uKarcmin2Jy(self, sigmanoise):
        """
        compute the :math:`\sigma` detection flux limit  from the sensitivity in :math:`\mu K arcmin`.
        """
        sigmanoise *= u.uK * u.arcmin
        T0 = 2.725 * u.K
        x = (C.h.cgs * self.nu.to(u.Hz) / (C.k_B.cgs * T0)).decompose()
        uKamin2Jy = (
            2
            * x ** 4
            * C.k_B.cgs ** 3
            * T0 ** 2
            / (C.h.cgs ** 2 * C.c.cgs ** 2)
            / (4 * np.sinh(x.value / 2) ** 2)
            / u.sr
        ).to(u.Jy / u.uK / u.arcmin ** 2)
        return 2 * np.sqrt(self.omega_b).to(u.arcmin) * sigmanoise * uKamin2Jy

    def uKsrts2Jy(self, sigmanoise):
        """
        Compute the :math:`\sigma` detection flux limit  from the sensitivity in :math:`\mu K \sqrt{s}` computed from the
        Noise Effective Temperature computed on the whole array of focal plane detectors.

        """

        Nbeams = self.fsky / self.omega_b  # nbeams within the observational patch
        noiseT = sigmanoise * u.uK / np.sqrt(self.timeobs.value) * np.sqrt(Nbeams)
        K2Jy = (
            2.0 * C.k_B.cgs * (self.nu.to(u.Hz) / C.c.cgs) ** 2 * self.omega_b.value
        ).to(u.Jy / u.uK)
        return K2Jy * noiseT

    def Jyperbeam2Jy(self, sigmanoise):
        """
        Compute the :math:`\sigma` detection flux limit  from the brightness ensitivity.
        """
        sigmanoise *= u.Jy / u.sr
        return sigmanoise * self.omega_b

    def print_info(self):

        c = bash_colors()
        print(c.header("===" * 30))
        print(c.bold(self.ID + "\tSpecifics\n"))
        print("Frequency\t......", self.nu)
        print("Flux limit\t......", self.flux_lim)
        print("Resolution\t......", self.fwhm)
        print("# channels\t......", self.nchans)
        print("Fraction of sky\t......", self.fsky / 4.0 / np.pi / u.sr)
        print("Beam angle\t......", self.omega_b)

    def check_format(self):
        tmp = self.nu
        self.nu = np.zeros(1) * tmp.unit
        self.nu.itemset(tmp)
        tmp = self.fwhm
        self.fwhm = np.zeros(1) * tmp.unit
        self.fwhm.itemset(tmp)
        tmp = self.omega_b
        self.omega_b = np.zeros(1) * tmp.unit
        self.omega_b.itemset(tmp)
        tmp = self.flux_lim
        self.flux_lim = np.zeros(1) * tmp.unit
        self.flux_lim.itemset(tmp)

    def __init__(
        self,
        ID,
        sensitivity,
        frequency,
        fwhm,
        fsky,
        units_sensitivity="uKarcmin",
        units_beam="arcmin",
        time_observation=None,
    ):

        dic_units_beam = {"deg": u.deg, "arcmin": u.arcmin, "arcsec": u.arcsec}
        self.ID = ID
        if time_observation is not None:
            self.timeobs = time_observation.to(u.s)
        self.fsky = fsky * 4 * np.pi * u.sr
        dic_converter = {
            "uKarcmin": self.uKarcmin2Jy,
            "uKsqrts": self.uKsrts2Jy,
            "Jyperbeam": self.Jyperbeam2Jy,
            "Jy": lambda f: f * u.Jy,
            "mJy": lambda f: (f * u.mJy).to(u.Jy),
        }

        if type(frequency) is list:
            self.nchans = len(frequency)
            # sort frequency channels
            idsort = np.argsort(frequency)
            self.fwhm = np.array(fwhm)[idsort] * dic_units_beam[units_beam]
            self.nu = np.array(frequency)[idsort] * u.gigahertz
            self.omega_b = [beamsolidangle(f).value for f in self.fwhm] * u.sr
            self.sigmanoise = np.array(sensitivity)[idsort]
            self.flux_lim = dic_converter[units_sensitivity](self.sigmanoise)
        else:
            self.nchans = 1
            self.nu = frequency * u.gigahertz
            self.fwhm = fwhm * dic_units_beam[units_beam]
            self.sigmanoise = sensitivity
            self.omega_b = beamsolidangle(self.fwhm)

            self.flux_lim = dic_converter[units_sensitivity](self.sigmanoise)
            self.check_format()
