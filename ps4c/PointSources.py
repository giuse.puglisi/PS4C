#
#   PointSources.PY
#   module to initialize point source collections.
#
#   date: 2017-12-21
#   author: GIUSEPPE PUGLISI
#
#   Copyright (C) 2017   Giuseppe Puglisi    giuspugl@sissa.it
#
import numpy as np


class PointSources(object):
    def assign_polerr(self, sigmaP, sigmaPuK):
        self.polerr = sigmaP
        self.polerr_uK = sigmaPuK

    def assign_polflux(self, pol_frac):
        fluxerr = self.polerr
        if self.resolved:
            flux = self.gauflux
        else:
            flux = self.detflux
        if self.polflux != 0.0:
            print(
                "Polarization detected for %s with polarization fraction=%g "
                % (self.id, self.polflux / flux)
            )
            pass
        else:
            polflux = flux * pol_frac
            if polflux > fluxerr:
                self.polflux = polflux
                self.polangle = np.random.uniform(low=0, high=np.pi)

        pass

    def forecast_polarization(self, pol_frac):
        """
		Assign polarized flux to the source from forecasts, could be either a
		constant value to all the clouds or a value drawn from a distribution.
		"""
        fluxerr = self.polerr
        if self.resolved:
            flux = self.gauflux
        else:
            flux = self.detflux
        if self.polflux != 0.0:
            print("passing", self.polflux / flux, self.id)
            pass
        else:
            polflux = flux * pol_frac
            if polflux < fluxerr:
                self.polflux = polflux
                self.polangle = np.random.uniform(low=0, high=np.pi)
            else:
                self.unpolarized = True

        pass

    def get_pol_frac(self):

        if self.resolved:
            return self.polflux / self.gauflux
        else:
            return self.polflux / self.detflux

    def get_polfrac_error(self):
        P = self.polflux
        sigmaP = self.polerr
        if self.resolved:
            I = self.gauflux
            sigmaI = self.gauerr
        else:
            I = self.detflux
            sigmaI = self.deterr
        return sigmaf(P, I, sigmaP, sigmaI)

    def __iter__(self):
        for attr, value in self.__dict__.iteritems():
            yield attr, value

    def __init__(self, idx, catalog, ps_dict, pol_sensitive=True):
        """
		Initialize the point source from the ID of  a catalogue read as an input.

		**Parameters**

		- idx:{int, or None }
		 	ID in the catalogue
		- catalogue: list of arrays
		- ps_dict: dictionary
			associate values  to each point source quantity
		.. note::
			if `idx` is `None` initialize the class  via the dictionary `ps_dict`  which  reades point sources property
			saved in an `hdf5` file (see :func:`read_ps_selection_from_hdf5`).
		"""

        if idx is not None:
            self.id = catalog[ps_dict["id"]][idx]
            self.glon = catalog[ps_dict["glon"]][idx]
            self.glat = catalog[ps_dict["glat"]][idx]
            self.ra = catalog[ps_dict["ra"]][idx]
            self.dec = catalog[ps_dict["dec"]][idx]

            self.detflux = catalog[ps_dict["detflux"]][idx]
            self.deterr = catalog[ps_dict["deterr"]][idx]

            self.gauflux = catalog[ps_dict["gauflux"]][idx]
            self.gauerr = catalog[ps_dict["gauerr"]][idx]

            self.omega_eff = catalog[ps_dict["omega_eff"]][idx]
            self.resolved = bool(catalog[ps_dict["resolved"]][idx])
            self.ext_val = catalog[ps_dict["ext_val"]][idx]
            if pol_sensitive:
                self.polflux = catalog[ps_dict["polflux"]][idx]
                self.polerr = catalog[ps_dict["polerr"]][idx]

                if self.polflux != 0:
                    self.polangle = catalog[ps_dict["polangle"]][idx]
                    self.polang_err = catalog[ps_dict["polang_err"]][idx]

                else:
                    self.polangle = np.nan
                    self.polang_err = catalog[ps_dict["polang_err"]][idx]

        else:
            self.dict = {i: k for i, k in ps_dict.items()}

            self.id, self.glon, self.glat, self.ra, self.dec, self.omega_eff = (
                self.dict["id"],
                self.dict["glon"],
                self.dict["glat"],
                self.dict["ra"],
                self.dict["dec"],
                self.dict["omega_eff"],
            )
            self.detflux, self.gauflux, self.deterr, self.gauerr = (
                self.dict["detflux"],
                self.dict["gauflux"],
                self.dict["deterr"],
                self.dict["gauerr"],
            )
            self.polflux, self.polerr, self.polangle, self.polang_err = (
                self.dict["polflux"],
                self.dict["polerr"],
                self.dict["polangle"],
                self.dict["polang_err"],
            )
            self.resolved = self.dict["resolved"]
            self.ext_val = self.dict["ext_val"]
        self.unpolarized = False
