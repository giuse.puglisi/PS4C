#
#   PS4Cast.PY
#   module encoding the forecasting package.
#
#   date: 2017-12-21
#   author: GIUSEPPE PUGLISI
#
#   Copyright (C) 2017   Giuseppe Puglisi    giuspugl@sissa.it
#


import pylab as pl
import healpy as hp
import numpy as np
import h5py as h5
import glob
import scipy as sp
from scipy import interpolate
from scipy import integrate
from scipy.interpolate import interp1d
import scipy.optimize as so
from scipy.optimize import minimize_scalar
import string
import astropy
from astropy import units as u, constants as C
from astropy.modeling.blackbody import blackbody_nu
import matplotlib
from matplotlib import rcParams

rcParams.update({"figure.autolayout": True})

from .experiment import Experiment
from .pointsource_utilities import (
    compute_polfrac_scaling,
    compute_dNdP,
    compute_confusion_limit,
    fit_numbercounts_w_powerlaw,
    fit_numbercounts_w_2powerlaws,
    linfunc,
    estimate_power_spectrum_contribution,
    extrapolate_number_counts,
    brightness2Tcmb,
    forecast_Galaxy,
    distance_from_coord_center,
    gaussian_apodization,
)

from .Stats import fitting_lognormal_from_fpol, fitting_lognormal_from_fluxes, pi2

from .IO import bash_colors, read_number_counts_DSFG, read_number_counts


class Forecaster(object):
    """
    Given the specifics of an experiment, it  forecasts the contribution of detected and undetected point sources (only Radio Quasars are considered so far). It inheritates the attributes from the :class:`Experiment`, needed to be initialized, the level
    of point source detection. The forecasts start when the :class:`Forecaster` class is called.

    **Parameters**

    - `Experiment`:{:class:`Experiment`}

    - `sigmadetection`:{float}
        set the level above one can consider point sources as *detected*.
    - `ps4c_dir`:{string}
        path to the ``PS4C/data`` and ``PS4C/model`` folders


    """

    def __init__(self, Experiment, sigmadetection=5.0, ps4c_dir="./"):
        self.ID = Experiment.ID
        self.Exp = Experiment

        self.nu = Experiment.nu
        self.fwhm = Experiment.fwhm
        self.omega_b = Experiment.omega_b
        self.fsky = Experiment.fsky
        self.flux_lim = sigmadetection * Experiment.flux_lim
        self.sigma_detection = sigmadetection
        self.nchans = Experiment.nchans
        self.workingdir = ps4c_dir

    def print_info(self):
        self.Exp.print_info()
        c = bash_colors()
        print(c.header("///" * 30))
        print(c.bold("Forecasted quantities for Radio Galaxies \n"))
        print(
            "Frequency \t#sources[S,P]\t Detection \tConfusion\t <Pi> \t <Pi^2>x1e3 \tD^TT(lensing)\tD^BB(lensing)\n"
        )
        for i in np.sort(list(self.Pi.keys())):
            print(
                "{0}\t{4}\t{5}\t{6.value:g} {6.unit:FITS}\t{1.value:g} {1.unit:FITS}\t{2:.2f}\t{3:.2f}\t".format(
                    i * u.GHz,
                    self.flux_conf[i].to(u.mJy),
                    self.Pi[i],
                    self.Pi2[i] * 1e3,
                    self.nradiosources[i],
                    self.npolsources[i],
                    self.detection_flux[i].to(u.mJy),
                ),
                " {0.value:g} {0.unit:FITS}\t  {1.value:g} {1.unit:FITS}".format(
                    self.C_detdsources[i] * 1e6 / 2.0 / np.pi / u.sr,
                    self.C_poldet[i] * 1e6 / 2.0 / np.pi / u.sr,
                ),
            )
        print(c.header("===" * 30))
        print(c.bold("Forecasted quantities for Dusty Star Forming  Galaxies \n"))
        print(
            "Frequency \t#sources[S,P]\tDetection \t    <Pi^2>x1e3 \tD^TT(lensing)\t D^TT (clustering, lens.)\tD^BB(lensing)\n"
        )
        for i in np.sort(list(self.pi2dust.keys())):
            print(
                "{0}\t{2}\t{3}\t{4.value:g} {4.unit:FITS}\t{1:.2f} \t".format(
                    i * u.GHz,
                    self.pi2dust[i] * 1e3,
                    self.ndustsources[i],
                    self.npoldustsources[i],
                    self.detection_flux[i].to(u.mJy),
                ),
                " {0.value:g} {0.unit:FITS}\t  {2.value:g} {1.unit:FITS}\t {1.value:g} {1.unit:FITS} ".format(
                    self.C_detdustsources[i] * 1e6 / 2.0 / np.pi / u.sr,
                    self.C_poldust[i] * 1e6 / 2.0 / np.pi / u.sr,
                    self.C_clustering[i] * 1000 ** 0.8 / 2.0 / np.pi / u.sr,
                ),
            )
        print(c.header("===" * 30))

    def plot_powerspectra(self, spectra_to_plot="all", FG="compsep", **kwargs):
        """
        Plot CMB and undetected Point sources Power spectra.

        **Parameters**

        - `spectra_to_plot`:{string}
            a string among:

            - `all`: T, E and B spectra will be shown
            -  `TandB`: T and B spectra
            -   `Tonly`: T spectrum
            -   `Bonly`: B spectrum

        - `FG`:{string}
            show the Galactic foreground  levels, a string among:

            - `compsep`: the 95perc  of  residuals  after component separation
            - `total`: total amplitude
            - `none`:  will not be shown

        - `kwargs`:
            keywords for matplotlib plots.
        """
        dic_to_plot = {
            "all": ["TT", "EE", "BB"],
            "TandB": ["TT", "BB"],
            "Tonly": ["TT"],
            "Bonly": ["BB"],
            "Eonly": ["EE"],
        }
        dic_spectra = {
            "all": range(3),
            "TandB": [0, 2],
            "Tonly": [0],
            "Bonly": [2],
            "Eonly": [1],
        }
        dic_subplot = {
            "all": (3, 1),
            "TandB": (2, 1),
            "Tonly": (1, 1),
            "Bonly": (1, 1),
            "Eonly": (1, 1),
        }
        FGfactor = {"compsep": 0.01, "total": 1, "none": 0}
        ell2 = lambda x: x * (x + 1) / 2.0 / np.pi

        lens = np.array(hp.read_cl(self.workingdir + "data/lensedCls.fits"))

        l = np.arange(len(lens[-1]))
        tens = np.array(hp.read_cl(self.workingdir + "data/r_0.05_tensCls.fits"))
        subplot = dic_subplot[spectra_to_plot]
        parameters = self.nu.value
        norm = matplotlib.colors.Normalize(
            vmin=np.min(parameters), vmax=np.max(parameters)
        )

        c_m = matplotlib.cm.viridis
        s_m = matplotlib.cm.ScalarMappable(cmap=c_m, norm=norm)
        s_m.set_array([])
        fwhm = min(self.fwhm)
        idsort = np.argsort(self.nu.value)
        ziplist = zip(self.nu[idsort].value, parameters)
        pl.title(self.ID, fontsize=12)  # +' '+str(nu)+' GHz')
        J = 0
        for I in dic_spectra[spectra_to_plot]:
            pl.subplot(subplot[0], subplot[1], J + 1)
            for nu, p in ziplist:
                ellmax = np.pi / (fwhm).to(u.rad).value
                ell = np.arange(1, ellmax)
                pl.loglog(l, ell2(l) * (lens[I] + tens[I]), "k")
                if I == 2:
                    pl.loglog(l, ell2(l) * lens[I], "k-.")
                    pl.loglog(l, ell2(l) * tens[I], "k-.")
                    pl.loglog(
                        ell,
                        FGfactor[FG]
                        * forecast_Galaxy(ell, nu * u.GHz, self.fsky).value,
                        ":",
                        alpha=0.8,
                        lw=3,
                        color=s_m.to_rgba(p),
                    )
                try:
                    if I == 0:

                        pl.loglog(
                            ell,
                            ell2(ell) * self.C_confsources[nu],
                            "-.",
                            lw=2,
                            color=s_m.to_rgba(p),
                        )
                        pl.loglog(
                            ell,
                            ell2(ell) * self.C_detdsources[nu],
                            "--",
                            lw=2,
                            color=s_m.to_rgba(p),
                        )

                    if nu > 225:
                        # plot also DSFG
                        if I == 0:
                            pl.loglog(
                                ell,
                                (ell) ** 0.8 * self.C_clustering[nu],
                                "-.",
                                lw=2,
                                color=s_m.to_rgba(p),
                            )
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_detdustsources[nu],
                                "--",
                                dashes=(10, 20),
                                lw=2,
                                color=s_m.to_rgba(p),
                            )
                        else:
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_poldust[nu],
                                "--",
                                dashes=(10, 20),
                                lw=2,
                                color=s_m.to_rgba(p),
                            )
                    elif nu < 225:
                        if I == 0:
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_confsources[nu],
                                ":",
                                lw=4,
                                color=s_m.to_rgba(p),
                            )
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_detdsources[nu],
                                "--",
                                lw=2,
                                color=s_m.to_rgba(p),
                            )
                        else:
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_poldet[nu],
                                "--",
                                lw=2,
                                color=s_m.to_rgba(p),
                            )
                    else:
                        if I == 0:
                            pl.loglog(
                                ell,
                                (ell) ** 0.8 * self.C_clustering[nu],
                                "-.",
                                lw=2,
                                color=s_m.to_rgba(p),
                            )
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_detdustsources[nu],
                                "--",
                                lw=2,
                                dashes=(10, 20),
                                color=s_m.to_rgba(p),
                            )
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_confsources[nu],
                                ":",
                                lw=4,
                                color=s_m.to_rgba(p),
                            )
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_detdsources[nu],
                                "--",
                                lw=2,
                                color=s_m.to_rgba(p),
                            )
                        else:
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_poldust[nu],
                                "--",
                                dashes=(10, 20),
                                color=s_m.to_rgba(p),
                            )
                            pl.loglog(
                                ell,
                                ell2(ell) * self.C_poldet[nu],
                                "--",
                                lw=2,
                                color=s_m.to_rgba(p),
                            )
                except KeyError:
                    print(f"can't plot spectra at {nu}  GHz")
            J += 1
            pl.yticks(fontsize=14)
            pl.ylabel(r"$\mathcal{D}_\ell\, [\mu K ^2]$", fontsize=19)
            pl.tight_layout()
            if I == 2 or dic_spectra[spectra_to_plot] == "Tonly":
                pl.xticks(fontsize=14)
                pl.xlabel(r"$\ell$", fontsize=17)
            # else:
            #    pl.xticks(visible=False )
        if "ylim" in kwargs:
            pl.ylim(kwargs["ylim"])
        if "xlim" in kwargs:
            pl.xlim(kwargs["xlim"])
        else:
            pl.xlim([10, ellmax])
        if "savefig" in kwargs:
            pl.savefig(kwargs["savefig"])
        else:
            pl.show()

        pl.close("all")

    def plot_integral_numbercounts(self, **kwargs):
        """
        Plot integral number counts, i.e.   number of sources above the detection flux per square degree units.

        """
        parameters = self.nu.value
        # norm is a class which, when called, can normalize data into the
        # [0.0, 1.0] interval.
        norm = matplotlib.colors.Normalize(
            vmin=np.min(parameters), vmax=np.max(parameters)
        )

        # choose a colormap
        c_m = matplotlib.cm.viridis

        # create a ScalarMappable and initialize a data structure
        s_m = matplotlib.cm.ScalarMappable(cmap=c_m, norm=norm)
        s_m.set_array([])
        Integralcount = lambda integrand, Smin, Smax: sp.integrate.quad(
            integrand, Smin, Smax, limit=1000, epsrel=1.0e-3
        )[0]
        idsort = np.argsort(self.nu)
        ziplist = zip(self.nu[idsort].value, parameters)
        # ziplist=zip(self.S.keys()[idsort],  parameters)
        for nu, p in ziplist:
            try:
                fluxes = self.S[nu][1::3]
                Ncounts = [
                    Integralcount(self.dNdS[nu], Smin.value, self.Smax.value)
                    / u.sr.to((u.deg) ** 2)
                    for Smin in fluxes
                ]
                pl.loglog(
                    fluxes,
                    Ncounts,
                    label=str(int(nu)) + " GHz - radio",
                    color=s_m.to_rgba(p),
                )
            except KeyError:
                print(f"can't plot number counts  for radio sources at {nu} ")
            try:
                fluxes = self.Sdust[nu][1::3]
                Ncounts = [
                    Integralcount(
                        self.dNdSdust[nu], Smin.value, self.Sdust[nu].max().value
                    )
                    / u.sr.to((u.deg) ** 2)
                    for Smin in fluxes
                ]
                pl.loglog(
                    fluxes,
                    Ncounts,
                    "--",
                    label=str(int(nu)) + " GHz - dusty",
                    color=s_m.to_rgba(p),
                )
            except KeyError:
                print(f"can't plot number counts  for dusty sources at {nu} ")
        pl.legend(loc="best", fontsize=12)
        pl.xlabel(r"$S_{cut}$ [ Jy ]", fontsize=12)
        pl.ylabel(r"$N(>S) [ \, deg^{-2}\, ]$", fontsize=12)
        pl.yticks(fontsize=13)
        pl.xticks(fontsize=13)
        if "ylim" in kwargs:
            pl.ylim(kwargs["ylim"])
        if "xlim" in kwargs:
            pl.xlim(kwargs["xlim"])

        pl.tight_layout()
        pl.grid(True)
        if "savefig" in kwargs:
            pl.savefig(kwargs["savefig"])
        else:
            pl.show()

    def forecast_pi2scaling(self, **kwargs):
        """
        Compute the best fit parameters of the linear fit :math:`< Pi^2 >  = A x +B`
        """
        # compute the frequency scaling of the <Pi2(freq)> = A*freq +B, to forecast at any frequency
        self.A, self.B, self.sigmaA, self.sigmaB = compute_polfrac_scaling(
            self.workingdir + "data/", **kwargs
        )
        #

    def forecast_nsources_in_the_patch(self):
        """
        Given the size of the observational patch it computes how many sources should be detected above the detection flux limit.
        """
        self.nradiosources = {}
        self.ndustsources = {}

        Integralcount = lambda integrand, Smin, Smax: sp.integrate.quad(
            integrand, Smin, Smax, limit=1000, epsrel=1.0e-3
        )[0]

        idsort = np.argsort(self.nu.value)
        ziplist = zip(self.nu[idsort].value, self.flux_lim[idsort])

        for nu, Smin in ziplist:

            if nu <= 200:

                if Smin < self.S[nu].min():
                    Smin = self.S[nu].min()
                totcounts = (
                    Integralcount(self.dNdS[nu], Smin.value, self.Smax.value) / u.sr
                )
                self.nradiosources[nu] = int((totcounts) * self.fsky)
                self.ndustsources[nu] = None

            elif nu > 200:  # and nu <= 230:
                if Smin < self.S[nu].min():
                    Smin = self.S[nu].min()
                totcounts = (
                    Integralcount(self.dNdS[nu], Smin.value, self.Smax.value) / u.sr
                )
                self.nradiosources[nu] = int((totcounts) * self.fsky)
                if Smin < self.Smindust[nu]:
                    Smin = self.Smindust[nu]
                if self.Smax > self.Sdust[nu].max():
                    Smax = self.Sdust[nu].max()
                else:
                    Smax = self.Smax
                totdustcounts = (
                    Integralcount(self.dNdSdust[nu], Smin.value, Smax.value) / u.sr
                )
                self.ndustsources[nu] = int((totdustcounts) * self.fsky)
            """else:
                self.nradiosources[nu] = None
                if Smin < self.Smindust[nu]:
                    Smin = self.Smindust[nu]
                if self.Smax > self.Sdust[nu].max():
                    Smax = self.Sdust[nu].max()
                else:
                    Smax = self.Smax
                totdustcounts = (
                    Integralcount(self.dNdSdust[nu], Smin.value, Smax.value) / u.sr
                )
                self.ndustsources[nu] = int((totdustcounts) * self.fsky)"""

    def get_npolsources_from_flux(self, minflux=None):
        """
        By default, it computes how many polarized  sources should be observed above the  detection limit
        otherwise the sources whose flux >``minflux``.
        """

        Integralcount = lambda integrand, Smin, Smax: sp.integrate.quad(
            integrand, Smin, Smax, limit=1000, epsrel=1.0e-3
        )[0]
        if minflux is None:
            self.npolsources = {}

            idsort = np.argsort(self.nu.value)
            ziplist = zip(self.nu[idsort].value, self.flux_lim[idsort].value)
            for nu, Pmin in ziplist:
                Pmin *= u.Jy
                if self.nradiosources[nu] == None:
                    self.npolsources[nu] = None
                    continue
                else:
                    if Pmin <= self.P[nu].min():
                        Pmin = self.P[nu].min()
                    totcounts = (
                        Integralcount(self.dNdP[nu], Pmin.value, self.P[nu].max().value)
                        / u.sr
                    )
                    self.npolsources[nu] = int((totcounts) * self.fsky)
            return self.npolsources
        else:
            idsort = np.argsort(self.nu.value)
            ziplist = zip(self.nu[idsort].value, [minflux] * self.nchans)
            npolsources = {}
            for nu, Pmin in ziplist:
                Pmin *= u.Jy
                totcounts = (
                    Integralcount(self.dNdP[nu], Pmin.value, self.P[nu].max().value)
                    / u.sr
                )
                npolsources[nu] = int((totcounts) * self.fsky)
            return npolsources

    def get_npol_DSFG(self):
        Integralcount = lambda integrand, Smin, Smax: sp.integrate.quad(
            integrand, Smin, Smax, limit=1000, epsrel=1.0e-3
        )[0]
        idsort = np.argsort(self.nu.value)
        ziplist = zip(self.nu[idsort].value, self.flux_lim[idsort].value)
        self.npoldustsources = {}
        for nu, Smin in ziplist:
            if self.ndustsources[nu] == None:
                self.npoldustsources[nu] = None
                continue
            else:
                totcounts = (
                    Integralcount(self.dNdPdust[nu], Smin, self.Pdust[nu].max().value)
                    / u.sr
                )
                self.npoldustsources[nu] = int((totcounts) * self.fsky)

    def get_differential_number_counts_dezotti(self):
        """
        Given the frequency channels finds the total intensity  number counts from predictions of  De Zotti et al. 2005 model (prediction at
        several frequencies are provided in `./PS4C/model` directory) and computes the polarization number counts by deconvolving
        the intensity number counts with the probability distribution of fractional polarization.
        """
        modeldir = self.workingdir + "model/dezotti/"
        model_avail = glob.glob(modeldir + "number_counts_*")
        frequency_model_avail = [
            (k.split("ghz")[0].split("_")[-1]) for k in model_avail
        ]
        c = bash_colors()

        self.dNdS = {}
        self.S = {}
        self.dNdP = {}
        self.P = {}
        datadir = self.workingdir + "data/"
        try:
            data_avail = glob.glob(datadir + "bestfit_params_*GHz*")
            data_avail[2]
        except IndexError:
            self.forecast_pi2scaling(bestfitparams2file=True)
            data_avail = glob.glob(datadir + "bestfit_params_*GHz*")

        bestfit_freqs = [i.split("GHz")[0].split("_")[-1] for i in data_avail]
        freq_selection = np.where(self.nu < 230 * u.GHz)[0]
        if len(freq_selection) == 0:
            print(
                c.warning(
                    "Radio galaxies  not accounted given the range of frequency you are forecasting. "
                )
            )
            pass
        else:

            for i, nu in enumerate(self.nu[freq_selection]):
                idxfreq = np.argmin(
                    abs(nu.value - np.array(list(map(float, frequency_model_avail))))
                )
                model = read_number_counts(
                    modeldir
                    + "number_counts_"
                    + frequency_model_avail[idxfreq]
                    + "ghz.dat"
                )
                S = np.power(10, model["#"]) * u.Jy
                differential_counts = (
                    np.power(10, model["Total \n"]) / np.sqrt(S ** (5)) / u.Jy / u.sr
                )
                if self.flux_lim[i] < S.min():
                    print(
                        c.warning(
                            f"Extrapolating number counts with a power law from {S.min()} to {self.flux_lim[i]}"
                        )
                    )
                    S, differential_counts = extrapolate_number_counts(
                        S, differential_counts, self.flux_lim[i]
                    )
                self.dNdS[nu.value] = interp1d(S, differential_counts)
                self.S[nu.value] = S
                self.Smin = S.min()
                idxparams = np.argmin(
                    abs(nu.value - np.array(list(map(float, bestfit_freqs))))
                )
                A, mu, sigma = np.load(data_avail[idxparams], allow_pickle=True)[0]
                self.P[nu.value], self.dNdP[nu.value] = compute_dNdP(
                    A, mu, sigma, self.dNdS[nu.value], self.S[nu.value]
                )
                self.P[nu.value] *= u.Jy

    def get_differential_number_counts_tucci(self):
        """
        Given the frequency channels finds the total intensity  number counts from predictions of  Tucci et al. 2011 model and computes the
        polarization number counts as in :func:`get_differential_number_counts`.
        """
        self.dNdS = {}
        self.S = {}
        self.dNdP = {}
        self.P = {}
        self.detection_flux = {}
        c = bash_colors()

        datadir = self.workingdir + "data/"
        try:
            data_avail = glob.glob(datadir + "bestfit_params_*GHz*")
            data_avail[2]
        except IndexError:
            self.forecast_pi2scaling(bestfitparams2file=True)
            data_avail = glob.glob(datadir + "bestfit_params_*GHz*")

        bestfit_freqs = [i.split("GHz")[0].split("_")[-1] for i in data_avail]

        for i, nu in enumerate(self.nu):
            self.detection_flux[nu.value] = self.flux_lim[i]
            model_avail = glob.glob(self.workingdir + "model/lagache/ns*_radio.dat")
            frequency_model_avail = [
                (k.split("ns")[1].split("_")[0]) for k in model_avail
            ]
            idxfreq = np.argmin(
                abs(nu.value - np.array(list(map(float, frequency_model_avail))))
            )
            tucci = np.loadtxt(model_avail[idxfreq])
            S = tucci[:, 0] * u.Jy
            differential_counts = tucci[:, 1] / u.Jy / u.sr
            if self.flux_lim[i] < S.min():
                print(
                    c.warning(
                        f"Extrapolating number counts with a power law from {S.min()} to {self.flux_lim[i]}"
                    )
                )
                S, differential_counts = extrapolate_number_counts(
                    S, differential_counts, self.flux_lim[i]
                )
            self.dNdS[nu.value] = interp1d(S, differential_counts)
            self.S[nu.value] = S
            self.Smin = S.min()

            idxparams = np.argmin(
                abs(nu.value - np.array(list(map(float, bestfit_freqs))))
            )
            A, mu, sigma = np.load(data_avail[idxparams], allow_pickle=True)[0]
            self.P[nu.value], self.dNdP[nu.value] = compute_dNdP(
                A, mu, sigma, self.dNdS[nu.value], self.S[nu.value]
            )
            self.P[nu.value] *= u.Jy

    def get_differential_number_counts_DSFG(self):
        """
        Given the frequency channels finds the total intensity  number counts from predictions of Cai et al. 2013 model
        (prediction at several frequencies are provided in `./PS4C/model` directory) . We include in total intensity
        number counts the contribution from clustering.  Then it computes the polarization number counts by convolving
        the intensity number counts with the probability distribution of fractional polarization.
        """

        datadir = self.workingdir + "data/"
        data_avail = glob.glob(datadir + "DSFG_bonavera17_lognormal_params_*GHz*")
        bestfit_freqs = [i.split("GHz")[0].split("_")[-1] for i in data_avail]

        modeldir = self.workingdir + "model/cai/"
        model_avail = glob.glob(modeldir + "DSFG_number_counts_*")
        frequency_model_avail = [
            (k.split("ghz")[0].split("_")[-1]) for k in model_avail
        ]
        c = bash_colors()

        self.dNdSdust = {}
        self.Sdust = {}
        self.dNdPdust = {}
        self.Pdust = {}
        self.Smindust = {}
        self.pi2dust = {}
        freq_selection = np.where(self.nu > 200.0 * u.GHz)[0]

        if len(freq_selection) == 0:
            print(
                c.warning(
                    "Star Forming galaxies not accounted given the range of frequency you are forecasting. "
                )
            )
            pass
        else:

            for i, nu in zip(freq_selection, self.nu[freq_selection]):
                idxfreq = np.argmin(
                    abs(nu.value - np.array(list(map(float, frequency_model_avail))))
                )
                model = read_number_counts_DSFG(
                    modeldir
                    + "DSFG_number_counts_"
                    + frequency_model_avail[idxfreq]
                    + "ghz.pkl"
                )
                S = model["# total"][0] * u.Jy
                differential_counts = (
                    model["# total"][1] / pl.sqrt(S.value ** (5)) / u.Jy / u.sr
                )
                if self.flux_lim[i] < S.min():
                    print(
                        c.warning(
                            f"Extrapolating number counts with a power law from {S.min()} to {self.flux_lim[i]}"
                        )
                    )
                    S, differential_counts = extrapolate_number_counts(
                        S, differential_counts, self.flux_lim[i]
                    )
                self.dNdSdust[nu.value] = interp1d(S, differential_counts)
                self.Sdust[nu.value] = S
                self.Smindust[nu.value] = S.min()
                idxparams = np.argmin(
                    abs(nu.value - np.array(list(map(float, bestfit_freqs))))
                )
                A, mu, errmu, sigma, errsigma = np.loadtxt(
                    data_avail[idxparams], comments="#", delimiter=","
                )
                self.Pdust[nu.value], self.dNdPdust[nu.value] = compute_dNdP(
                    A, mu, sigma, self.dNdSdust[nu.value], self.Sdust[nu.value]
                )
                self.pi2dust[nu.value] = pi2(mu / 100.0, sigma)
                self.Pdust[nu.value] *= u.Jy

    def __call__(self, model="Tucci", **kwargs):
        """
        The forecast session.

        **Parameters**

        - `model`:{string}
            if ``model!=D05`` it runs the Tucci et al.2011 model for number counts.
        - kwargs:
            keywords used by functions in :module:`Stats` and :module:`IO`: ``verbose=False``, ``saveplot=string`` , ``bestfitparams2file=True``.

        """
        # Read number counts from theoretical model
        c = bash_colors()
        if not "verbose" in kwargs:
            kwargs["verbose"] = False
        if model == "D05":
            self.get_differential_number_counts_dezotti()
        else:
            self.get_differential_number_counts_tucci()
        self.get_differential_number_counts_DSFG()
        self.Smax = 10.0 * u.Jy
        self.forecast_nsources_in_the_patch()

        self.get_npolsources_from_flux()
        self.get_npol_DSFG()

        self.flux_conf = {}
        self.Pi, self.errPi, self.Pi2 = {}, {}, {}

        self.C_detdsources, self.C_confsources, self.C_detdustsources = {}, {}, {}
        self.C_poldet, self.C_polconf = {}, {}
        self.C_clustering, self.C_poldust = {}, {}
        ziplist = zip(self.omega_b, self.nu.value, self.flux_lim)
        for beam, nu, flux_lim in ziplist:  # np.ndenumerate(np.sort(self.S.keys())) :
            # if nu < 230:
            K, Gamma = fit_numbercounts_w_powerlaw(
                self.S[nu], self.dNdS[nu], self.Smax, **kwargs
            )
            self.flux_conf[nu] = compute_confusion_limit(K, Gamma, beam)
            Pi = linfunc(nu * u.GHz, self.A, +self.B)
            errPi = linfunc(nu * u.GHz, self.A + self.sigmaA, self.B + self.sigmaB) - Pi
            Pi2 = ((Pi + errPi) * 1e-2) ** 2
            self.Pi2[nu] = Pi2
            self.errPi[nu] = errPi
            self.Pi[nu] = Pi
            # compute contribution  on TT, EE, BB
            try:
                self.C_detdsources[nu] = estimate_power_spectrum_contribution(
                    self.dNdS[nu], nu * u.GHz, beam, self.Smin.value, flux_lim.value
                )
            except ValueError:
                print(
                    c.warning(
                        "detection limit too low (below the model prediction), unable to compute contribution of undetected sources"
                    )
                )
                self.C_detdsources[nu] = 0.0
            try:
                self.C_confsources[nu] = estimate_power_spectrum_contribution(
                    self.dNdS[nu],
                    nu * u.GHz,
                    beam,
                    self.Smin.value,
                    self.flux_conf[nu].value,
                )
            except ValueError:
                print(
                    c.warning(
                        "confusion limit too low (below the model prediction), unable to compute contribution of confused sources"
                    )
                )
                self.C_confsources[nu] = 0.0
            self.C_poldet[nu] = 1.0 / 2.0 * self.Pi2[nu] * self.C_detdsources[nu]
            """else:
                if kwargs["verbose"]:
                    print(
                        c.warning(
                            "No available data of radio sources at frequencies above 217 GHz!\n We consider contribution from Dusty Star Forming Galaxies only"
                        )
                    )"""
            try:
                self.dNdSdust[nu]
                self.C_detdustsources[nu] = estimate_power_spectrum_contribution(
                    self.dNdSdust[nu],
                    nu * u.GHz,
                    beam,
                    self.Smindust[nu].value,
                    flux_lim.value,
                )
                nuref = 154.0 * u.gigahertz
                eps = lambda x: brightness2Tcmb(nuref) ** 2 / brightness2Tcmb(x) ** 2
                beta = np.random.uniform(
                    low=0.5, high=3.5
                )  # see the definition in sec. 5.4.3 Georges et al (2015)
                T = 20.0 * u.K
                eta = lambda x: x ** beta * blackbody_nu(nu, T)
                self.C_clustering[nu] = (
                    (
                        4
                        * eps(nu * u.gigahertz)
                        * (eta(nu * u.gigahertz) / eta(nuref)) ** 2
                    )
                    / 3000.0 ** (0.8)
                    * 2.0
                    * np.pi
                    * u.uK ** 2
                    * u.sr
                )

                self.C_poldust[nu] = (
                    1.0 / 2.0 * self.pi2dust[nu] * self.C_detdustsources[nu]
                )
            except KeyError:
                continue
        pass

    def from_Nsources_to_fluxlim(self, nu, Nsources):
        S = self.S[nu.value]
        dndS = self.dNdS[nu.value](S)

        Smax = S.max()
        nfl = interpolate.interp1d(
            np.log(S.value), np.log(dndS), fill_value="extrapolate", kind="linear"
        )
        nf = lambda S: np.exp(nfl(np.log(S)))
        S = np.logspace(np.log10(S.min().value), np.log10(S.max().value), np.int_(1e4 ) )
        n = nf(S)
        dS = np.diff(S)
        S = np.flip(S)
        n = np.flip(n)
        dS = np.flip(dS)
        S = S[1:]
        N = np.cumsum(n[1:] * dS) / u.sr * self.fsky  # N(>S) on the sky

        Nf = interpolate.interp1d(S, N, fill_value="extrapolate")
        Ntotbar = Nf(S.min()) - Nf(Smax)

        p = (Nf(S) - Nf(Smax)) / (Ntotbar)
        pinv = interpolate.interp1d(p, S, fill_value="extrapolate")
        S_min = pinv(Nsources / Ntotbar) * u.Jy

        return S_min
