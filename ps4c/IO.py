#
#   IO.PY
#   functions to read input catalogues and to save point source collections.
#
#   date: 2017-12-21
#   author: GIUSEPPE PUGLISI
#
#   Copyright (C) 2017   Giuseppe Puglisi    giuspugl@sissa.it
#


import healpy as hp
import numpy as np
import h5py as h5
import pickle
from .PointSources import *
from astropy.io import fits
import h5py
import warnings
import glob
from astropy import units as u
from astropy.io import fits, ascii

# warnings.simplefilter('ignore')


def extract_flux_from_planck_catalog(catalog, freq, **kwargs):
    """
    Read PCCS2 catalog at a given frequency.

    **Parameters**

    - `catalog`:{string}
    - `freq`:{float}

    """

    # pccs=hp.mrdfits(catalog)
    pccs = fits.open(catalog)[1]
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    # consider all the point sources found on the catalog
    LFI = True if freq <= 70 else False
    pol_sensitive = True if freq <= 353 else False

    Nsource, pslist = search_in_catalogue(
        pccs,
        -90,
        90,
        0,
        360.0,
        coord="Gal",
        LFI=LFI,
        pol_sensitive=pol_sensitive,
        verbose=kwargs["verbose"],
    )

    Iflux, Polflux, eI, eP = [], [], [], []
    for ps in pslist:
        if pol_sensitive:
            if ps.polflux == 0 or np.isnan(ps.polflux):
                continue
            else:
                Polflux.append(ps.polflux)
                eP.append(ps.polerr)
                if ps.resolved:
                    Iflux.append(ps.gauflux)
                    eI.append(ps.gauerr)
                else:
                    Iflux.append(ps.detflux)
                    eI.append(ps.deterr)
        else:
            if ps.resolved:
                Iflux.append(ps.gauflux)
                eI.append(ps.gauerr)
            else:
                Iflux.append(ps.detflux)
                eI.append(ps.deterr)

    if pol_sensitive:
        return Iflux, Polflux, eI, eP
    else:
        if kwargs["verbose"]:
            print("Warning : no polarization flux available! ")
        return Iflux, eI


def write_ps_selection_to_hdf5(ps_list, filename):
    """
    From a list of :class:`PointSources` save the list into a hdf5 binary file.
	"""

    f = h5.File(filename, "w")
    n_sources = len(ps_list)
    f.create_dataset(
        "N_sources", np.shape(n_sources), dtype=h5.h5t.STD_I32BE, data=n_sources
    )

    for p, idx in zip(ps_list, np.arange(n_sources)):
        g = f.create_group("PointSource_" + str(idx))
        d = dict(p)
        for k in d.keys():
            g.create_dataset(k, data=d[k])

    f.close()


def read_ps_selection_from_hdf5(filename):
    """
    Read a list of  :class:`PointSources` from a hdf5 binary file.
	"""
    f = h5.File(filename, "r")
    n_sources = f["N_sources"][...]
    ps_list = []

    for idx in range(n_sources):
        g = f["PointSource_" + str(idx)]
        d = {k: v[...] for k, v in g.items()}
        ps_list.append(PointSource(None, None, d))
    f.close()
    return ps_list


def project_sources2map(
    ps_list, nside, omega_beam, polarization=False, forecast_pol=None, muK=1e6
):
    """
    Project the list of  :class:`PointSources` into  a healpix map
	"""
    source_map = {"I": np.zeros(hp.nside2npix(nside))}
    if polarization:
        source_map = {
            "I": np.zeros(hp.nside2npix(nside)),
            "Q": np.zeros(hp.nside2npix(nside)),
            "U": np.zeros(hp.nside2npix(nside)),
        }
    cres = 0
    for p in ps_list:
        if forecast_pol is not None:
            p.forecast_polarization(forecast_pol)

        center = hp.ang2vec(np.deg2rad(90 - p.glat), np.deg2rad(p.glon))
        if p.resolved:
            cres += 1
            flux = p.gauflux
            radius = np.deg2rad(p.omega_eff / 60.0)  # the angle is in arcmin
        else:
            flux = p.detflux
            radius = np.deg2rad(omega_beam / 60.0)
        listpix = hp.query_disc(nside, center, radius)
        T_b = flux2brightness_temp(flux, np.pi * np.deg2rad(radius) ** 2)
        source_map["I"][listpix] = T_b * muK
        if polarization:
            tpol = flux2brightness_temp(p.fpol, np.pi * np.deg2rad(radius) ** 2)
            source_map["Q"][listpix] = tpol * np.cos(2.0 * p.polangle) * muK
            source_map["U"][listpix] = (
                -tpol * np.sin(2.0 * p.polangle) * muK
            )  # for this - sign check Planck 2015 PCCS2 paper

    print("extended sources %d" % cres)
    return [i for i in source_map.values()]


def search_in_catalogue(
    cat,
    minlat,
    maxlat,
    minlong,
    maxlong,
    coord="EQ",
    pol_sensitive=True,
    LFI=False,
    verbose=True,
):
    """
	Look for point sources within :math:`b_{min}< b<b_{max}` and :math:`\ell_{min}<\ell<\ell_{max}` in a catalogue .

	**Returns**

	- `counter`:{int}
		number of sources found
	- `point_sources`:{list}
		list of the point source found within the ranges

	"""
    if pol_sensitive and not LFI:
        dict_catalog = {
            "id": 0,
            "glon": 1,
            "glat": 2,
            "ra": 3,
            "dec": 4,
            "detflux": 5,
            "deterr": 6,
            "gauflux": 11,
            "gauerr": 12,
            "omega_eff": 19,
            "polflux": 20,
            "polerr": 21,
            "polangle": 22,
            "polang_err": 23,
            "resolved": 37,
            "ext_val": 38,
        }
    elif pol_sensitive and LFI:
        dict_catalog = {
            "id": 0,
            "glon": 1,
            "glat": 2,
            "ra": 3,
            "dec": 4,
            "detflux": 5,
            "deterr": 6,
            "gauflux": 11,
            "gauerr": 12,
            "omega_eff": 19,
            "polflux": 20,
            "polerr": 21,
            "polangle": 22,
            "polang_err": 23,
            "resolved": 30,
            "ext_val": 31,
        }
    else:
        dict_catalog = {
            "id": 0,
            "glon": 1,
            "glat": 2,
            "ra": 3,
            "dec": 4,
            "detflux": 5,
            "deterr": 6,
            "gauflux": 11,
            "gauerr": 12,
            "omega_eff": 19,
            "resolved": 20,
            "ext_val": 21,
        }
    catalogue = {v: cat.data[cat.columns[v].name] for v in dict_catalog.values()}

    n_sources = len(catalogue[0])
    counter = 0
    found = []

    #
    if coord == "EQ":
        x1, x2 = (catalogue[dict_catalog["ra"]]), (catalogue[dict_catalog["dec"]])
        limits = lambda glong, glat: (
            (glong <= minlong or maxlong <= glong)
            and (minlat <= glat and glat <= maxlat)
        )
    else:
        x1, x2 = (catalogue[dict_catalog["glon"]]), (catalogue[dict_catalog["glat"]])
        limits = lambda glong, glat: (
            (minlong <= glong <= maxlong) and (minlat <= glat <= maxlat)
        )

    for idx, glong, glat in zip(range(n_sources), x1, x2):

        if limits(glong, glat):
            found.append(idx)
            counter += 1
        else:
            continue
    if verbose:
        print(
            " Found %d  sources within  deg=(%.1f,%.1f),ra=(%.1f,%.1f) ."
            % (counter, (minlat), (maxlat), (minlong), (maxlong))
        )

    point_sources = []
    for f in found:
        point_sources.append(
            PointSources(f, catalogue, dict_catalog, pol_sensitive=pol_sensitive)
        )
        # print( catalogue[dict_catalog['polerr']][f],catalogue[dict_catalog['polang_err']][f],catalogue[dict_catalog['deterr']][f],catalogue[dict_catalog['gauerr']][f])
    return counter, point_sources


def read_nvss_catalog(filename, **kwargs):
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    f = open(filename, "r")
    c = 0
    for l in f.readlines():
        cols = l.split("|", 27)[1:-1]
        c += 1
        if c == 3:
            nfields = len(cols)
            dict_catalog = {key: val for key, val in zip(cols, range(nfields))}
            catalog = {key: [] for key in dict_catalog.keys()}
        if c > 3:

            for k, i in dict_catalog.items():
                if i == 0:
                    catalog[k].append(cols[i])
                elif i == 1:  # ra
                    hh, mm, ss = cols[i].split(" ", 3)
                    angle = (
                        int(hh) * 15.0
                        + float(mm) * 15.0 / 60.0
                        + float(ss) * 15.0 / 3600
                    )

                    catalog[k].append(angle)
                elif i == 2:  # dec
                    dd, mm, ss = cols[i].split(" ", 3)
                    s = np.sign(int(dd))
                    angle = s * (s * int(dd) + float(mm) / 60.0 + float(ss) / 3600.0)
                    catalog[k].append(angle)

                else:
                    try:
                        catalog[k].append(float(cols[i]))
                    except ValueError:
                        catalog[k].append(np.nan)
    if kwargs["verbose"]:
        print("Read %d total sources from %s. " % (c - 3, filename))
    f.close()
    return catalog


def read_number_counts(filename, **kwargs):
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    f = open(filename, "r")
    c = 0
    # print( np.loadtxt(filename,  comments='#', delimiter=' '  ,skiprows=3, unpack=False))
    for l in f.readlines():
        if c == 1:
            cols = l.split("\t", 6)
            nfields = len(cols)
            dictd = {val: key for key, val in zip(cols, range(nfields))}
            data = {key: [] for key in dictd.values()}
        if c > 1:
            v = np.fromstring(l, sep=" ")
            for i in range(len(v)):
                data[dictd[i]].append(v[i])
        c += 1
    if not "verbose" in kwargs:
        print("Read %d  lines from   " % (c - 3, filename))
    f.close()
    return data


def read_spass_nvss_catalog(filename, **kwargs):
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    f = open(filename, "r")
    c = 0
    fields = []
    for l in f.readlines():
        li = l.strip()
        if (
            not li.startswith("#")
            and not li.startswith("---")
            and not li.startswith("deg")
        ):
            cols = l.split(";")[2:-3]
            # print( cols)
            if c == 1:
                # print( cols)
                fields = cols
                nfields = len(fields)
                dict_catalog = {k: v for k, v in zip(fields, range(nfields))}
                catalog = {k: [] for k in fields}
                # print( catalog)
            elif c > 1:
                v = [np.fromstring(i, sep=";") for i in cols]
                # print( v, len(v)==nfields #fields.append(cols[4]))
                for i, k in zip(v, fields):
                    if k == fields[0]:
                        catalog[k].append(cols[0])
                    else:
                        catalog[k].append(i)
            c += 1
    f.close()
    # print( catalog['NVSS'])
    for k in catalog.keys():
        if k == "NVSS":
            continue
        catalog[k] = np.concatenate(catalog[k])
    if kwargs["verbose"]:
        print("Read %d total sources from %s " % (len(catalog["NVSS"]), filename))
    return catalog, dict_catalog


def read_galluzzi_fpol_dat(filename, **kwargs):
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    arr = np.loadtxt(filename)
    flag = np.array([a[0] for a in arr])
    errpi = np.array([a[2] / 100.0 for a in arr])
    pi = np.array([a[1] / 100.0 for a in arr])
    if kwargs["verbose"]:
        print("Read %d lines from %s " % (len(flag), filename))
    return flag, pi, (errpi)


def read_jvas_catalog(filename, **kwargs):
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    dic = {
        "ra": 0,
        "dec": 1,
        "z": 2,
        "I": 3,
        "eI": 4,
        "P": 5,
        "eP": 6,
        "fpol": 7,
        "efpol": 8,
        "PA": 9,
        "ePA": 10,
    }
    loadfile = np.loadtxt(filename)
    dicdata = {i: [] for i in dic.keys()}
    for row in loadfile:
        if row[dic["fpol"]] >= 100:
            if kwargs["verbose"]:
                print("Skipping Weird value of Pi", row[dic["fpol"]])
            continue
        for i in dic.keys():
            dicdata[i].append(row[dic[i]])
    dicdata = {i: np.array(dicdata[i]) for i in dic.keys()}
    if kwargs["verbose"]:
        print("Read %d lines from %s" % (len(dicdata["ra"]), filename))
    return dicdata


def read_atca_catalog(filename, **kwargs):
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    f = open(filename, "r")
    c = 0
    fields = []
    for l in f.readlines():
        li = l.strip()
        if (
            not li.startswith("#")
            and not li.startswith("---")
            and not li.startswith("deg")
        ):
            cols = l.split(";")
            if c == 1:
                fields = cols
                nfields = len(fields)
                dict_catalog = {k: v for k, v in zip(fields, range(nfields))}
                catalog = {k: [] for k in fields}
            elif c > 2:
                v = [np.fromstring(i, sep=";") for i in cols]
                if (
                    v[dict_catalog["m5\n"]] > 100
                    or v[dict_catalog["m8"]] > 100
                    or v[dict_catalog["m20"]] > 100
                ):
                    if kwargs["verbose"]:
                        print(
                            "Skipping Weird value of Pi",
                            v[dict_catalog["m5\n"]],
                            v[dict_catalog["m8"]],
                            v[dict_catalog["m20"]],
                        )
                    continue

                for k, i in dict_catalog.items():

                    if i == 0 or i == 9:
                        catalog[k].append(cols[i])
                    elif i == 1:  # ra
                        hh, mm, ss = cols[i].split(" ", 3)
                        angle = (
                            int(hh) * 15.0
                            + float(mm) * 15.0 / 60.0
                            + float(ss) * 15.0 / 3600
                        )
                        catalog[k].append(angle)
                    elif i == 2:  # dec
                        dd, mm, ss = cols[i].split(" ", 3)
                        s = np.sign(int(dd))
                        angle = s * (
                            s * int(dd) + float(mm) / 60.0 + float(ss) / 3600.0
                        )
                        catalog[k].append(angle)
                    else:
                        try:
                            catalog[k].append(float(cols[i]))
                        except ValueError:

                            catalog[k].append(np.nan)
            c += 1
            # for k in catalog.keys():#if c==2000:
    catal = {k: np.array(catalog[k]) for k in catalog.keys()}  # break
    if kwargs["verbose"]:
        print("Read %d rows from the AT20G catalog" % c)
    f.close()

    return catal, dict_catalog


class bash_colors:

    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"

    def header(self, string):
        return self.HEADER + str(string) + self.ENDC

    def blue(self, string):
        return self.OKBLUE + str(string) + self.ENDC

    def green(self, string):
        return self.OKGREEN + str(string) + self.ENDC

    def warning(self, string):
        return self.WARNING + str(string) + self.ENDC

    def fail(self, string):
        return self.FAIL + str(string) + self.ENDC

    def bold(self, string):
        return self.BOLD + str(string) + self.ENDC

    def underline(self, string):
        return self.UNDERLINE + str(string) + self.ENDC


def read_IRAM_catalog(filename, **kwargs):
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    f = open(filename, "r")
    c = 0
    fields = []
    for l in f.readlines():
        li = l.strip()
        if (
            not li.startswith("#")
            and not li.startswith("---")
            and not li.startswith("deg")
        ):
            cols = l.split(";")
            if c == 0:
                fields = cols
                nfields = len(fields)
                dict_catalog = {k: v for k, v in zip(fields, range(nfields))}
                catalog = {k: [] for k in fields}
            else:
                v = [np.fromstring(i, sep=";") for i in cols]
                strings = ["Name", "l_mL", "l_mC"]
                for k, i in dict_catalog.items():
                    if strings.__contains__(k):
                        catalog[k].append(cols[i])
                    else:
                        catalog[k].append(v[i][0])

            c += 1
    for k in catalog.keys():
        catal = {k: np.array(catalog[k]) for k in catalog.keys()}  # break
    if kwargs["verbose"]:
        print("Read %d rows from the AT20G catalog" % c)

    f.close()

    return catal, dict_catalog


def read_VLA_catalog(filename, **kwargs):
    if not "verbose" in kwargs:
        kwargs["verbose"] = False
    f = open(filename, "r")
    c = 0
    fields = []
    for l in f.readlines():
        li = l.strip()
        if (
            not li.startswith("#")
            and not li.startswith("---")
            and not li.startswith("deg")
        ):
            cols = l.split(";")
            if c == 0:
                fields = cols
                nfields = len(fields)
                dict_catalog = {k: v for k, v in zip(fields, range(nfields))}
                catalog = {k: [] for k in fields}
            else:
                v = [np.fromstring(i, sep=";") for i in cols]
                strings = [
                    "AT20G",
                    "m_AT20G",
                    "l_pC",
                    "l_pX",
                    "l_pK",
                    "l_pQ",
                    "Class\n",
                ]

                for k, i in dict_catalog.items():
                    if strings.__contains__(k):
                        catalog[k].append(cols[i])
                    else:
                        catalog[k].append(v[i][0])

            c += 1
    for k in catalog.keys():  # if c==2000:
        catal = {k: np.array(catalog[k]) for k in catalog.keys()}  # break
    if kwargs["verbose"]:
        print("Read %d rows from the AT20G catalog" % c)

    f.close()

    return catal, dict_catalog


def read_number_counts_DSFG(filename, **kwargs):
    """
    Number counts of DSFG from Cai 2013 are organized in the *.pkl file as a dictionary with
    # each key encoding the flux in mJy and the euclidean number counts .
    """

    with open(filename, "rb") as f:
        catalog = pickle.load(f, encoding="latin1")
    #### we 've to convert mJy to Jy
    for k in catalog.keys():
        catalog[k][0] *= 1.0e-3

    f.close()
    return catalog


def get_alpha_catalog(nu, path, string=""):
    alpha_files = glob.glob(path + "Spectral_indices_matches_*")

    if nu > 217 * u.GHz and string == "radio":
        tab = ascii.read(path + "Spectral_indices_matches_143_217.dat")
        warnings.warn(
            f"Forecasting spectral indices for {string} sources  at {nu} from data at 143-217 GHz "
        )
        f1 = 217

        return tab, [f1]
    elif nu < 217 * u.GHz and string == "dusty":
        tab = ascii.read(path + "Spectral_indices_matches_353_545.dat")
        warnings.warn(
            f"Forecasting spectral indices for {string} sources at {nu} from data at 353-545  GHz "
        )
        f1 = 217
        return tab, [f1]

    else:
        if nu < 30 * u.GHz:
            tab = ascii.read(path + "Spectral_indices_matches_30_44.dat")
            warnings.warn(
                f"Forecasting spectral indices for {string} sources at {nu} from data at 30-40 GHz "
            )

            f1 = 30
            return tab, [f1]

        elif nu >= 857 * u.GHz:
            tab = ascii.read(path + "Spectral_indices_matches_545_857.dat")
            warnings.warn(
                f"Forecasting spectral indices for {string} sources at {nu} from data at 545-857  GHz "
            )
            f1 = 857
            return tab, [f1]

        else:
            for file in alpha_files:
                f1 = np.float_(file.split("_")[-2]) * u.GHz

                f2 = np.float_(file.split("_")[-1].split(".dat")[0]) * u.GHz
                if f1 <= nu and nu < f2:
                    tab = ascii.read(file)

                    break

            return tab, [f1.value, f2.value]


def write_h5_file(filename, dataset):
    f = h5py.File(filename, "w")
    for k, v in (dataset).items():
        f.create_dataset(k, np.shape(v), dtype=h5.h5t.IEEE_F64BE, data=v)
    f.close()


def read_h5_file(filename, dataset):
    f = h5py.File(filename, "r")
    data_dic = {}
    for k in dataset:
        data_dic[k] = f[k][...]
    f.close()
    return data_dic
