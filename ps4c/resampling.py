import numpy as np
from .pointsource_utilities import fit_numbercounts_w_2powerlaws
from .IO import bash_colors, get_alpha_catalog
from scipy.signal import find_peaks
from astropy import units as u

from scipy import interpolate


def get_random_fluxes_inverse_resampling(S, dndS, Smin, fsky, nsources=None):
    """
    Credits to Marcelo Alvarez
    """
    dndS = dndS(S)
    Smax = S.max()
    nfl = interpolate.interp1d(
        np.log(S.value), np.log(dndS), fill_value="extrapolate", kind="linear"
    )
    nf = lambda S: np.exp(nfl(np.log(S)))
    Na = 100000  # for interpolation,it should be large enough (at least >1000 )

    S = np.logspace(np.log10(S.min().value), np.log10(S.max().value), Na)
    n = nf(S)
    dS = np.diff(S)
    S = np.flip(S)
    n = np.flip(n)
    dS = np.flip(dS)
    S = S[1:]
    N = np.cumsum(n[1:] * dS) / u.sr * fsky  # N(>S) on the sky

    Nf = interpolate.interp1d(S, N, fill_value="extrapolate")
    Ntotbar = np.int(Nf(Smin) - Nf(Smax))
    p = (Nf(S) - Nf(Smax)) / (Nf(Smin) - Nf(Smax))

    pinv = interpolate.interp1d(p, S, fill_value="extrapolate")

    if nsources is not None:
        Ntot = nsources
    else:
        Ntot = np.random.poisson(Ntotbar)  # draw a poisson number from Ntotbar

    r = np.random.rand(np.int(Ntot))  # uniform  Ntot random numbers from 0,1

    Srand = pinv(r)

    return Srand * u.Jy


def get_random_fluxes_from2powerlaw(
    S, dndS, Smin, nvals, S_pivot=1 * u.Jy, verbose=False
):
    s0 = S_pivot.value
    # if Smax is None:
    Smax = S.max()

    xvals = np.random.uniform(size=np.int_(nvals))

    k1, alpha1 = fit_numbercounts_w_2powerlaws(
        S, dndS, range=[0.01 * u.Jy, S_pivot], verbose=False
    )
    k2, alpha2 = fit_numbercounts_w_2powerlaws(
        S, dndS, range=[S_pivot, Smax], verbose=False
    )

    alpha1 *= -1
    alpha2 *= -1
    k1 *= 1 / (u.Jy ** (1 + alpha1))
    k2 *= 1 / (u.Jy ** (1 + alpha2))

    X1 = k1 / (alpha1 + 1) * (S_pivot ** (alpha1 + 1) - Smin ** (alpha1 + 1))
    X2 = k2 / (alpha2 + 1) * (Smax ** (alpha2 + 1) - S_pivot ** (alpha2 + 1))

    ntot = X1 + X2

    maskgreater = np.ma.masked_greater(xvals, X2 / ntot).mask

    # if x <= X2/ntot
    inversefunc_less = lambda x: (
        Smax ** (alpha2 + 1) - (alpha2 + 1) / k2 * (x * ntot)
    ) ** (1.0 / (alpha2 + 1))
    # else
    inversefunc_grtr = lambda x: (
        S_pivot ** (alpha1 + 1) - (alpha1 + 1) / k1 * (x * ntot - X2)
    ) ** (1.0 / (alpha1 + 1))

    Srand = np.zeros_like(xvals)
    Srand[maskgreater] = inversefunc_grtr(xvals[maskgreater])
    Srand[~maskgreater] = inversefunc_less(xvals[~maskgreater])

    return Srand * u.Jy


def bootstrapping_array(arr, nresamples=5000, nbins=50, randseed=None, string=""):
    density = True
    if randseed is not None:
        np.random.seed(randseed)

    h0, bins2 = np.histogram(arr, bins=nbins, density=density)
    binxs2 = np.array([0.5 * (bins2[i] + bins2[i + 1]) for i in range(len(bins2) - 1)])
    peak_ids = find_peaks(h0, height=0.4, distance=10)[0]
    q1 = np.quantile(q=0.84, a=arr)
    q2 = np.quantile(q=0.16, a=arr)
    if len(peak_ids) > 1:
        string_dic = {"radio": [0], "dusty": [1], "": [0, 1]}

        peaks = binxs2[peak_ids][string_dic[string]]

        sigma = np.ones_like(peaks) * arr.std() / 2.7
        # we divide by 2.7  to replicate the width of the 2-peak distribution @ 217-353
        nsub = len(string_dic[string])
        size = (
            np.int_(nresamples / nsub),
            nsub,
        )  # size.shape= (  nresamples/2 ,2) if len(peaks)=2
    else:
        peaks = np.median(arr)
        sigma = 0.5 * (q1 - q2)  #
        size = nresamples
    bstrap = np.random.normal(loc=peaks, scale=sigma, size=size)

    return bstrap


def get_reference_frequency(f_range, nu):

    f_range = np.array(f_range)
    if len(f_range) == 2:
        return f_range[np.argmin(np.fabs(f_range - nu))] * u.GHz
    elif len(f_range) == 1:
        return f_range[0] * u.GHz


def rescale_fluxes_with_powerlaw(fluxes, nu, id_string, datadir):
    # get spectral index distr.
    tab, f_range = get_alpha_catalog(nu, path=datadir, string=id_string)
    # get nuref from the range the spectral index  is estimated

    nuref = get_reference_frequency(f_range, nu.value)

    alpha_bootstrap = bootstrapping_array(
        tab["ALPHA"], nresamples=fluxes.shape[0], string=id_string
    )
    fluxes = fluxes * (nu / nuref) ** alpha_bootstrap
    return fluxes
