from astropy import units as u
import glob
from scipy import interpolate
from astropy.io import ascii
import numpy as np
from scipy.signal import find_peaks


class SimulateSED:
    """
    Credits to Marcelo Alvarez
    """

    def __init__(self, datadir, id_string):
        self.datadir = datadir
        if self.datadir[-1] != "/":
            self.datadir += "/"
        self.source_kind = id_string
        data = self.get_alpha_values()
        nu = data[:, 0]
        alpha_bar = data[:, 1]
        sigma = data[:, 2]

        alpha_bar = interpolate.interp1d(nu, alpha_bar, fill_value="extrapolate")
        sigma = interpolate.interp1d(nu, sigma, fill_value="extrapolate")

        Nnu = 200

        nu1 = 4.0
        nu2 = 910.0

        lnnu = np.linspace(np.log(nu1), np.log(nu2), Nnu)
        dlnnu = lnnu[1] - lnnu[0]
        nu = np.exp(lnnu)

        dlnf0 = alpha_bar(nu) * dlnnu
        dlnf1 = sigma(nu) * dlnnu

        lnf0a = dlnf0.cumsum()
        lnf1a = dlnf1.cumsum()

        self.lnf0 = interpolate.interp1d(nu, lnf0a)
        self.lnf1 = interpolate.interp1d(nu, lnf1a)

    def get_alpha_median_sigma(self, arr, nbins=50):
        density = True

        h0, bins2 = np.histogram(arr, bins=nbins, density=density)
        binxs2 = np.array(
            [0.5 * (bins2[i] + bins2[i + 1]) for i in range(len(bins2) - 1)]
        )
        peak_ids = find_peaks(h0, height=0.4, distance=10)[0]
        q1 = np.quantile(q=0.84, a=arr)
        q2 = np.quantile(q=0.16, a=arr)

        if len(peak_ids) > 1:
            string_dic = {"radio": [0], "dusty": [1]}

            peaks = binxs2[peak_ids][string_dic[self.source_kind]]

            sigma = np.ones_like(peaks) * arr.std() / 2.7
            # we divide by 2.7  to replicate the width of the 2-peak distribution @ 217-353
            #nsub = len(string_dic[string])
            #size = (
            #    np.int_(nresamples / nsub),
            #    nsub,
            #)  # size.shape= (  nresamples/2 ,2) if len(peaks)=2
        else:
            peaks = np.median(arr)
            sigma = 0.5 * (q1 - q2)  #
        return peaks, sigma

    def get_alpha_values(self):
        alpha_files = glob.glob(self.datadir + "Spectral_indices_matches_*")
        # get spectral index distr.
        med, nu, sigma = [], [], []

        for file in alpha_files:
            f1 = np.float_(file.split("_")[-2])
            f2 = np.float_(file.split("_")[-1].split(".dat")[0])
            nu.append(np.sqrt(f1 * f2))
            if np.sqrt(f1 * f2) > 217 and self.source_kind == "radio":
                file = self.datadir + "Spectral_indices_matches_143_217.dat"
            elif np.sqrt(f1 * f2) < 217 and self.source_kind == "dusty":
                file = self.datadir + "Spectral_indices_matches_353_545.dat"

            vals = self.get_alpha_median_sigma(ascii.read(file)["ALPHA"])
            med.append(vals[0])
            sigma.append(vals[1])
        return np.array([nu, med, sigma]).T

    def fluxfunc(self):
        r = np.random.normal()
        return lambda nu: np.exp(self.lnf0(nu) + r * self.lnf1(nu))

    def initialize_random_values(self, nsources):
        self.r = np.random.normal(size=nsources)

    def get_SEDs(self, nu):
        return np.exp(self.lnf0(nu) + self.r * self.lnf1(nu))
