import healpy as hp
import numpy as np
import h5py as h5
import glob

from .forecaster import Forecaster
from .sed_modelling import SimulateSED
from .resampling import (
    get_random_fluxes_from2powerlaw,
    get_random_fluxes_inverse_resampling,
    bootstrapping_array,
    rescale_fluxes_with_powerlaw,
)
from .pointsource_utilities import (
    extrapolate_number_counts,
    flux2temp,
    normalize_bandpass_weights,
    forecast_Galaxy,
    distance_from_coord_center,
    gaussian_apodization,
)
from .IO import (
    bash_colors,
    read_number_counts_DSFG,
    read_number_counts,
    get_alpha_catalog,
    write_h5_file,
)
import string
import astropy
from astropy import units as u, constants as C
from scipy.interpolate import interp1d


class CatalogForecaster(Forecaster):
    def __init__(self, Experiment, sigmadetection=5.0, ps4c_dir="./"):
        super(Forecaster, self).__init__()
        self.ID = Experiment.ID
        self.Exp = Experiment
        self.nchans = Experiment.nchans
        self.nu = Experiment.nu
        self.fwhm = Experiment.fwhm
        self.omega_b = Experiment.omega_b

        self.fsky = Experiment.fsky
        self.flux_lim = sigmadetection * Experiment.flux_lim
        self.sigma_detection = sigmadetection
        self.nchans = Experiment.nchans
        self.workingdir = ps4c_dir

    def make_radio_source_map(
        self,
        nside=512,
        verbose=False,
        store_maps=False,
        outdir=".",
        output_units=u.uK,
        antenna_temp=False,
    ):
        self.pmap = np.zeros(hp.nside2npix(nside) * self.nchans * 3).reshape(
            hp.nside2npix(nside), 3, self.nchans
        )

        for i in range(self.nchans):
            nu = self.nu[i]
            nuv = nu.value
            omega_b = self.omega_b[i]
            fwhm = self.fwhm[i]
            # estimate brightness temperature from fluxes
            if self.bpasses  is None :
                Tb = flux2temp(flux =self._fluxes[nuv], freq= nu,
                    omega_b = omega_b, antenna=antenna_temp).to(
                    output_units
                )
            else :
                Tb = flux2temp(flux =self._fluxes[nuv], freq= self.bpasses [nuv][0] ,
                    omega_b = omega_b, antenna=antenna_temp,
                    bandpass_weights=self.bpasses [nuv][1] ).to(
                    output_units
                )


            if self.has_polarization:
                # estimate polarization angle uniformly  from -pi/2 to pi/2
                if self.bpasses  is None :
                    Pb = flux2temp(
                        flux =self._Pfluxes[nuv], freq= nu,omega_b= omega_b, antenna=antenna_temp
                    ).to(output_units)
                else :
                    Pb = flux2temp(flux =self._Pfluxes[nuv], freq= self.bpasses [nuv][0] ,
                        omega_b = omega_b, antenna=antenna_temp,
                        bandpass_weights=self.bpasses [nuv][1] ).to(
                        output_units
                    )

                psirand = np.random.uniform(
                    low=-np.pi / 2.0, high=np.pi / 2.0, size=self._fluxes[nuv].shape[0]
                )
                cosrand = np.cos(2 * psirand)
                sinrand = np.sin(2 * psirand)
                # Estimate Q and U  stokes
                Qb = Pb * cosrand
                Ub = Pb * sinrand
                for v, T, Q, U in zip(self._hpx_vecs[nuv], Tb, Qb, Ub):
                    listpix = hp.query_disc(
                        nside=nside, vec=v, radius=3 * fwhm.to(u.rad).value
                    )
                    theta_pix, phi_pix = hp.pix2ang(nside, listpix)
                    theta_c, phi_c = hp.vec2ang(v)
                    distances = distance_from_coord_center(
                        theta_pix, phi_pix, theta_c, phi_c
                    )
                    profile = gaussian_apodization(distances, fwhm.to(u.rad).value)
                    self.pmap[listpix, 0, i] += profile * T.value
                    self.pmap[listpix, 1, i] += profile * Q.value
                    self.pmap[listpix, 2, i] += profile * U.value

            else:
                for v, T in zip(self._hpx_vecs[nuv], Tb):

                    listpix = hp.query_disc(
                        nside=nside, vec=v, radius=3 * fwhm.to(u.rad).value
                    )
                    theta_pix, phi_pix = hp.pix2ang(nside, listpix)
                    theta_c, phi_c = hp.vec2ang(v)
                    distances = distance_from_coord_center(
                        theta_pix, phi_pix, theta_c, phi_c
                    )
                    profile = gaussian_apodization(distances, fwhm.to(u.rad).value)
                    self.pmap[listpix, 0, i] += profile * T.value
            # if verbose :print(f"Average time  to perform the per source  projection  ( nside= {nside})  Healpix map: { np.mean(tmp )}"  )
            if store_maps:
                for i in range(self.nchans):
                    hp.write_map(
                        f"{outdir}/source_maps_{self.ID}_{self.nu[i].value:.0f}ghz.fits",
                        [self.pmap[:, iter, i] for iter in range(3)],
                        overwrite=True,
                    )

    def resampling_catalog(self, coords, nu, fluxes, all_sources, lonlat, sort_fluxes):
        if lonlat:
            latrange = [-90, 90]
            lonrange = [0, 360]
        else:
            latrange = [0, np.pi]
            lonrange = [0, 2 * np.pi]

        if (
            all_sources   and fluxes is None
        ):  # simulate fluxes for ALL the entries in the catalog
            glats = coords[1]
            glons = coords[0]
            nsources = glats.shape[0]
            S = self.S[nu.value]
            nb = self.dNdS[nu.value](S) / u.Jy / u.sr
            S, nb = extrapolate_number_counts(S, nb, flux_lim=(1e-12 * u.Jy))

            self.dNdS[nu.value] = interp1d(S, nb)
            self.S[nu.value] = S

            # estimate flux_lim  given Nsources
            S_min = self.from_Nsources_to_fluxlim(nu, nsources)

            if self.verbose:
                print(f"Considering {S_min:.1e}  as minimum flux to resample ")
            fluxes = get_random_fluxes_inverse_resampling(
                S=self.S[nu.value],
                dndS=self.dNdS[nu.value],
                Smin=S_min,
                fsky=self.fsky,
                nsources=nsources,
            )[:nsources]
        elif all_sources  and fluxes is not None:
            if self.verbose:
                print(" Using the catalog as it is. No resampling ")
            ## use the catalog AS is, no need to sort
            nsources = coords[1].shape[0]
            try:
                fluxes.value
            except AttributeError:
                fluxes *= u.Jy
            return nsources, coords[0], coords[1], fluxes

        elif not all_sources and fluxes is None:
            # resample Flux

            nsources = self.nradiosources[nu.value]
            if self.verbose:
                print(
                    f"No input  fluxes => Simulating fluxes with  bootstrap resamples",
                    f"at {nu }.",
                )
            fluxes = get_random_fluxes_inverse_resampling(
                S=self.S[nu.value],
                dndS=self.dNdS[nu.value],
                Smin=self.detection_flux[nu.value],
                fsky=self.fsky,
            )
            nsources = fluxes.shape[0]
            try:
                coords[0][nsources]
                # the line above is to raise the IndexError
                # if nsources > coords.shape[0] python by default won't raise an error ,
                #  coords [:nsources,1 ]  corresponds to  coords [:-1,1 ]

                glats = coords[1][:nsources]
                glons = coords[0][:nsources]  #

                if self.verbose:
                    print(f"using {nsources} coordinates  from catalog ")

            except IndexError:
                ncatalog = len(coords[0])
                if self.verbose:
                    print(f"generating  {nsources - ncatalog } random coordinates  ")

                glats = np.concatenate(
                    [
                        coords[1],
                        np.random.uniform(
                            low=latrange[0], high=latrange[1], size=nsources - ncatalog,
                        ),
                    ]
                )
                glons = np.concatenate(
                    [
                        coords[0],
                        np.random.uniform(
                            low=lonrange[0], high=lonrange[1], size=nsources - ncatalog,
                        ),
                    ]
                )

        else:
            # i.e. not all_sources and fluxes is not  None
            nsources = self.nradiosources[nu.value]
            try:
                fluxes.value
            except AttributeError:
                fluxes *= u.Jy
            try:
                # nsources< ncatalog
                coords[1][nsources]
                idx_sort = np.argsort(fluxes)[::-1][:nsources]
                fluxes = fluxes[idx_sort]
                glats = coords[1][idx_sort]
                glons = coords[0][idx_sort]
                if self.verbose:
                    print(f"Using {nsources  }  sorted fluxes  from catalog")
            except IndexError:

                # nsources>  ncatalog resampling sources  nsource - ncatalog
                ncatalog = len(coords[0])

                if self.verbose:
                    print(
                        f"Bootstrap resampling  fluxes for  {nsources - fluxes.shape[0]} sources"
                    )
                # in this case we don't need to sort the fluxes
                randomfluxes = get_random_fluxes_inverse_resampling(
                    S=self.S[nu.value],
                    dndS=self.dNdS[nu.value],
                    Smin=self.detection_flux[nu.value],
                    fsky=self.fsky,
                    nsources=nsources - fluxes.shape[0],
                )
                fluxes = np.concatenate([fluxes, randomfluxes])
                glats = np.concatenate(
                    [
                        coords[1],
                        np.random.uniform(
                            low=latrange[0], high=latrange[1], size=nsources - ncatalog,
                        ),
                    ]
                )
                glons = np.concatenate(
                    [
                        coords[0],
                        np.random.uniform(
                            low=lonrange[0], high=lonrange[1], size=nsources - ncatalog,
                        ),
                    ]
                )

        # Sort all the fluxes in descending order
        if sort_fluxes:
            idx_sort = np.argsort(fluxes)[::-1]
            fluxes = fluxes[idx_sort]
            glats = glats[idx_sort]
            glons = glons[idx_sort]
        # print(fluxes.shape, nsources )
        assert glats.shape[0] == nsources
        assert glons.shape[0] == nsources
        assert fluxes.shape[0] == nsources

        return nsources, glons, glats, fluxes

    def forecast_radio_source_fluxes(
        self,
        coords,
        lonlat=True,
        rescale_fluxes=False,
        fluxes=None,
        nuref=90,
        simulate_polarization=True,
        all_sources=False,
        verbose=False,
        sort_fluxes=False,
        store_fluxes=False,
        outdir=".",
        bandpasses=None,
    ):
        """

         - coords :
            2D array with coordinates (from a catalog )
            longitude :coords[:,0]
            latitude :coords[:,1]

        """
        self.has_polarization = simulate_polarization
        self.verbose = verbose
        self._fluxes = {}
        self._hpx_vecs = {}
        self._Pfluxes = {}
        self.bpasses = bandpasses
        if self.has_polarization:
            self._Pfluxes = {}
        dataset_dic = {}
        datadir = self.workingdir + "data/"

        if self.nu.isscalar:
            self.check_format()

        if rescale_fluxes:
            seds = {}
            SED = SimulateSED(datadir, id_string="radio")
            nuref *= u.GHz
            indexmin = np.argmin(np.fabs(self.nu - nuref))
            nurefv = self.nu[indexmin].value
            if not all_sources:
                self.detection_flux[nurefv] /=100
            nsources, glons, glats, self._fluxes[nurefv] = self.resampling_catalog(
                coords=coords,
                nu=self.nu[indexmin],
                fluxes=fluxes,
                all_sources=all_sources,
                lonlat=lonlat,
                sort_fluxes=sort_fluxes,
            )
            if not all_sources :
                self.detection_flux[nurefv] *=100
            SED.initialize_random_values(len(self._fluxes[nurefv]))
            sed_nuref = SED.get_SEDs(nurefv)
            idlabel = "spectral_index_matched_catalog"
        else:
            indexmin = None
            nurefv = None
            idlabel = "abundance_matched_catalog"

        for i in range(self.nchans):
            nu = self.nu[i]
            nuv = nu.value
            omega_b = self.omega_b[i]
            fwhm = self.fwhm[i]
            if rescale_fluxes and nuv != nurefv:
                # Rescale fluxes
                print(
                    f"Rescaling flux with Planck Spectral Indices from {nurefv} to {nuv} GHz"
                )

                if self.bpasses  is None:
                    sed_nu = SED.get_SEDs(nuv)
                    self._fluxes[nuv] = sed_nu / sed_nuref * self._fluxes[nurefv]
                else:
                    self._fluxes[nuv] = self.integrate_bandpass(
                        nuv, SED, sed_nuref, self._fluxes[nurefv]
                    )
                assert self._fluxes[nurefv].unit == self._fluxes[nuv].unit


            elif not rescale_fluxes and nuv != nurefv:
                print("Not Rescaling flux with Planck Spectral Indices ")
                nsources, glons, glats, self._fluxes[nuv] = self.resampling_catalog(
                    coords=coords,
                    nu=nu,
                    fluxes=fluxes,
                    all_sources=all_sources,
                    lonlat=lonlat,
                    sort_fluxes=sort_fluxes,
                )
            else:
                print(f"skipping resampling at reference freq. ")
                # Integrate the bandpasses also for the channel at nuref

                if self.bpasses  is not None:
                    self._fluxes[nuv] = self.integrate_bandpass(
                        nuv, SED, sed_nuref, self._fluxes[nurefv]
                    )
                    assert self._fluxes[nurefv].unit == self._fluxes[nuv].unit

            if lonlat:
                dataset_dic[f"GLON"] = glons
                dataset_dic[f"GLAT"] = glats
            else:
                dataset_dic[f"PHI"] = glons
                dataset_dic[f"THETA"] = glats

            dataset_dic["FLUXES_Jy"] = self._fluxes[nuv]
            if self.has_polarization:
                self.Qb = {}
                self.Ub = {}
                data_avail = glob.glob(datadir + "bestfit_params_*GHz*")
                # get Pol. frac distribution
                bestfit_freqs = [i.split("GHz")[0].split("_")[-1] for i in data_avail]
                idxparams = np.argmin(
                    abs(nuv - np.array(list(map(float, bestfit_freqs))))
                )
                _, mu, sigma = np.load(data_avail[idxparams], allow_pickle=True)[0]
                fpol = (
                    np.random.lognormal(mean=np.log(mu), sigma=sigma, size=nsources)
                    / 100
                )
                self._Pfluxes[nuv] = fpol * self._fluxes[nuv]
                dataset_dic["POL_FLUX_Jy"] = self._Pfluxes[nuv]

            if lonlat:
                self._hpx_vecs[nu.value] = hp.ang2vec(
                    theta=(glons), phi=glats, lonlat=lonlat
                )
            else:
                self._hpx_vecs[nu.value] = hp.ang2vec(
                    theta=(glats), phi=glons, lonlat=lonlat
                )
            if store_fluxes:
                write_h5_file(
                    f"{outdir}/{idlabel}_{nuv:.0f}ghz.h5", dataset=dataset_dic
                )

    def integrate_bandpass(self, central_freq, SED, sedref, Sref):
        weights = self.bpasses[central_freq][1]
        freqs = self.bpasses[central_freq][0]
        nfreqs = len(freqs)
        # normalize weights
        weigths = normalize_bandpass_weights( weights, freqs)
        tmp = np.zeros((len(sedref), nfreqs)) * Sref.unit
        for i, nu in enumerate(freqs):
            sednu = SED.get_SEDs(nu)
            tmp[:, i] = sednu / sedref * Sref * weights[i]

        integrated_sed = np.trapz(tmp, freqs, axis=1)
        assert integrated_sed.shape == sedref.shape
        return integrated_sed

    
